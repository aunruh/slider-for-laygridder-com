module.exports = function(grunt) {

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		jshint: {
			files: ['Gruntfile.js', '*.js'],
			options: {
				globals: {
					jQuery: true,
					console: true
				}
			}
		},
		concat: {
			frontend: {
			    // the files to concatenate
			    src: [
			    	'frontend/js/flickity.js',
			    	'frontend/js/app.js',
			    ],
			    // the location of the resulting JS file
			    dest: 'frontend/js/bundle/bundle.js'
			},
		},
		sass: {
			frontend: {
				files: {
					'frontend/css/style.css':'frontend/scss/style.scss'
				}
			},
		},
		postcss: {
			options:{
				processors: [ 
					require('autoprefixer')({browsers: 'last 5 versions'}), // add vendor prefixes 
				]
			},
			frontend: {
				src: 'frontend/css/style.css'
			},
		},
		watch: {
			frontendjs: {
				files: ['<%= concat.frontend.src %>'],
				tasks: ['concat:frontend'],
				options: {
			    	livereload: 35728,
			    }
			},
			frontendscss: {
				files: ['frontend/scss/*.scss'],
				tasks: ['sass:frontend', 'postcss:frontend'],
				options: {
			    	livereload: 35728,
			    }
			},
			reloadphp: {
				files: '**/*.php',
				options: {
					livereload: 35728
				}
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-livereload');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-postcss');

};