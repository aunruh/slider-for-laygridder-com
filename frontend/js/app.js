var sfl = (function(){

	var initFlickity = function(){
		jQuery('.slideshow').each(function(){
			(function(_this) {
				var $this = jQuery(_this);

				var $slideshow = $this.flickity({
					cellAlign: 'left',
					wrapAround: true,
					setGallerySize: false,
					percentPosition: false,
					pageDots: false,
					prevNextButtons: true,
					accessibility: false,
					draggable: true,
					selectedAttraction: 0.055,
					friction: 0.4,
					dragThreshold: 10
				});

			})(this);
			
		});
	};

	var init = function(){
		initFlickity();

	};

	return {
		init : init
	}

}());

jQuery(document).ready(function(){
	sfl.init();
});