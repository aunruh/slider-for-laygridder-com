<?php
/**
 * Plugin Name: Slider for laygridder.com
 * Plugin URI: 
 * Description: Example for a custom slideshow element for LayGridder
 * Version: 0.0.4
 * Author: Armin Unruh
 * Author URI: http://laygridder.com
 */

// http://wordpress.stackexchange.com/questions/127818/how-to-make-a-plugin-require-another-plugin
register_activation_hook( __FILE__, 'child_plugin_activate' );
function child_plugin_activate(){
    // Require parent plugin
    if ( !is_plugin_active('laygridder/laygridder.php') || !is_plugin_active('cmb2/init.php')  ) {
        // Stop activation redirect and show error
        wp_die('Sorry, but "Slider for LayGridder" requires "LayGridder" plugin and "CMB2" plugin to be installed and active. <br><a href="' . admin_url( 'plugins.php' ) . '">&laquo; Return to Plugins</a>');
    }
}


$sliderLGVer = '0.0.4';

define( 'SFL_PLUGIN_PATH', plugin_dir_path(__FILE__) );
define( 'SFL_PLUGIN_FILE_PATH', __FILE__ );
define( 'SFL_PLUGIN_URL', plugins_url( 'slider-for-laygridder-com' ));
define( 'SFL_VER', $sliderLGVer);

class SliderForLayGridder{
	public function __construct(){
		add_action( 'cmb2_admin_init', array( $this, 'sfl_register_metabox' ) );
		add_filter( 'lg_cmb2_modals',  array( $this, 'add_cmb2_to_laygridder'), 10, 1 );
		add_action( 'admin_enqueue_scripts', array($this, 'enqueue_slider_view_for_gridder') );
	

		add_filter( 'lg_frontend_sfl_metabox', array( $this, 'show_slider_on_frontend'), 10, 1);
		add_action( 'wp_enqueue_scripts', array($this, 'sfl_enqueue_js_frontend') );
		add_action( 'wp_enqueue_scripts', array($this, 'sfl_enqueue_css_frontend') );
	}

	public static function sfl_enqueue_js_frontend(){
		wp_enqueue_script( 'sfl-frontend-js', SFL_PLUGIN_URL.'/frontend/js/bundle/bundle.js', array('jquery'), SFL_VER );
	}

	public static function sfl_enqueue_css_frontend(){
		wp_enqueue_style( 'sfl-frontend-css', SFL_PLUGIN_URL.'/frontend/css/style.css', false, SFL_VER );
	}

	public static function show_slider_on_frontend( $element ){
		$slides = "";

		$sfl_file_list = $element->sfl_file_list->val;
		// error_log(print_r($sfl_file_list, true));
		$slideshow_ar = 16/9;

		foreach ($sfl_file_list as $img) {
			$attid = $img->id;
			// error_log($attid);
			$meta = wp_get_attachment_metadata($attid);
			$alt = get_post_meta($attid, '_wp_attachment_image_alt', true);
			$srcset = wp_get_attachment_image_srcset($attid);
			
			$img_ar = $meta["width"] / $meta["height"];
			$class = "w100";
			if($img_ar > $slideshow_ar){
				$class = "h100";
			}

			$slides .= 
			'<div class="slideshow-item">
				<img class="'.$class.'" srcset="'.$srcset.'" sizes="100vw" src="'.$img->url.'" alt="'.$alt.'">
			</div>';
		}

		return 
		'<div class="slideshow-wrap">
			<div class="slideshow-placeholder">
				<div class="slideshow">
				'.$slides.'
				</div>
			</div>
		</div>';
	}

	public static function enqueue_slider_view_for_gridder(){
		wp_enqueue_script( 'sfl-slider-gridder-view', SFL_PLUGIN_URL.'/gridder/js/view.js', false, SFL_VER );
	}

	public static function add_cmb2_to_laygridder( $metabox_form_ids ){
		$metabox_form_ids []= 'sfl_metabox';
		return $metabox_form_ids;
	}

	public static function sfl_register_metabox(){
		$cmb = new_cmb2_box( array(
			'id'            => 'sfl_metabox',
			'title'         => 'Slider',
		) );

		$cmb->add_field( array(
			'name'         => "Slider",
			'id'           => 'sfl_file_list',
			'type'         => 'file_list',
			'preview_size' => array( 100, 100 ), // Default: array( 50, 50 )
		) );
	}

}
new SliderForLayGridder();