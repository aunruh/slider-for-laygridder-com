var sfl_metabox_view = function(model){

	var viewContent = "";

	var file_list = model.get('sfl_file_list');
	if(file_list.val.length > 0){
		if(typeof file_list.val[0].url != "undefined" && file_list.val[0].url != ""){
			viewContent = '<img src="'+file_list.val[0].url+'" draggable="false" style="position:absolute; top:0; left:0; width:100%; height:100%; object-fit:cover;">';
		}
	}
	
	return String()
	+viewContent
	+'<div style="position:relative; padding-bottom:56.25%;"></div>'
	+'<div style="position:absolute; top:10px; left: 10px; font-size:20px; color: white; z-index: 1; background-color:black; padding: 5px;">'
		+'Slider'
	+'</div>';
};